---
title: Support
---

# Getting support with 4Geeks

4Geeks Support offers the following support services, that is available to 4Geeks platform customers:

- Basic Tier
- Standard Tier
- Premium Tier
- Integration Tier

## Basic Tier

Basic Tier is included for all 4Geeks platform customers. With Basic Tier,
you have access to this product documentation and [community forums](https://community.4geeks.io).

### Asking on the community forums

To ask questions on the community platform, please start an account and post a new topic in the Support category. Follow the next suggestions:

1. Choose a Clear and Descriptive Title.
    - Make sure your title accurately reflects the issue you're facing.
    - Use keywords that others might search for.

2. Provide Detailed Information:
    - Explain the problem you're encountering in as much detail as possible.
    - Include relevant screenshots or error messages.
    - Specify the steps you've already taken to try to resolve the issue.

3. Choose the Appropriate Category:
    - In this case, select the Support category since you're seeking assistance with a specific problem.

4. Post Your Topic:
    - Click the "Create a new topic" button.
    - Paste your title and detailed description into the provided fields.
    - Add any relevant tags to help others find your topic.
    - Click "Post" to submit your topic.

5. Monitor and Respond:
    - Keep an eye on your topic for replies from other community members or moderators.
    - Respond promptly to any questions or suggestions.


## Standard Tier

Standard Tier offers unlimited 1:1 technical support for outages and defects,
unexpected product behavior, product usage questions, billing issues, and feature
requests. The Standard Tier service is designed for small to medium organizations
with workloads under development. With Standard Support, you receive 8-hour response
times for Priority 2 (P2) cases.

Standard Tier has a fixed base service fee of $29 USD per month.

!!! note
    Standard Tier is a monthly subscription that auto-renews
    until canceled. If you cancel your 4Geeks Support subscription within
    the calendar month, you will receive and be charged for support for the remainder
    of the calendar month. 4Geeks reserves the right to refuse to provide support to
    any customer that frequently signs up for and then cancels 4Geeks Support.

To contract or cancel your 4Geeks Support subscription please contact sales.

[Contact sales](https://4geeks.io/contact){ .md-button }


## Premium Tier

Premium Tier offers unlimited 1:1 technical support for outages and defects,
unexpected product behavior, product usage questions, billing issues, feature
requests, and more. Receive 15-min response times for Priority 1 (P2) cases.

Premium Tier has a fixed base service fee of $800 USD per month.

!!! note
    Premium Tier is a monthly subscription that auto-renews
    until canceled. If you cancel your 4Geeks Support subscription within
    the calendar month, you will receive and be charged for support for the remainder
    of the calendar month. 4Geeks reserves the right to refuse to provide support to
    any customer that frequently signs up for and then cancels 4Geeks Support.

To contract or cancel your 4Geeks Support subscription please contact sales.

[Contact sales](https://4geeks.io/contact){ .md-button }


## Integration Tier

Integration Tier offers unlimited 1:1 engineering support for integrate,
migrate or modernize your current platform with 4Geeks products. It includes a
dedicated software engineer expert tailored to your business industry and tech stack.

Integration Support has a fixed base service fee of $6400 USD per month.

!!! note
    Integration Tier is a monthly subscription that auto-renews
    until canceled. If you cancel your 4Geeks Support subscription within
    the calendar month, you will receive and be charged for support for the remainder
    of the calendar month. 4Geeks reserves the right to refuse to provide support to
    any customer that frequently signs up for and then cancels 4Geeks Support.

To contract or cancel your 4Geeks Support subscription please contact sales.

[Contact sales](https://4geeks.io/contact){ .md-button }

---

## Language and Working Hours

4Geeks Support is available in the following languages:

- English
- Spanish

The language in which you write your case determines the language in which you receive support.

4Geeks Support is unavailable during regional holidays, unless your organization's
service includes 24/7 support.

English and Spanish support are available during hours of operation, which run from
8:00 AM on Monday to 5:00 PM on Friday, Central Standard Time (CST).
