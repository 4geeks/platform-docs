---
title: Plugins overview
sidebar_label: Overview
image: https://cc-og-image.vercel.app/Integrating payments on WooCommerce.png?theme=dark&md=1&fontFamily=source-sans-pro&fontSize=100px&images=https://storage.googleapis.com/mesmerizing-matrix-1380/4geeks-docs-logo.svg
keywords:
  - plugins
---


Plugins are additional software components that can be added to an existing software
application to extend its functionality. In many cases, plugins are created and
supported by a community of developers and users, rather than by the original
creators of the software.

In the case of 4Geeks, it is important to note that all plugins used in their
software applications are created and supported by the community, rather than
by 4Geeks themselves. This means that the plugins are typically open source and
freely available for use and modification by anyone.


Available plugins:

 - [WooCommerce](woocommerce)
 - [Odoo v13.0](https://apps.odoo.com/apps/modules/13.0/payment_4geeks/)
 - [Magento v2](https://github.com/Jupagar77/4geeks-magento2)


## Request a new plugin

If you are a user of a software application that relies on plugins to extend its
functionality, and you find that there is a particular feature or capability that
is not currently supported by any existing plugins, you may be able to request the
development of a new plugin to address your needs. [Contact us](https://4geeks.io/contact).
