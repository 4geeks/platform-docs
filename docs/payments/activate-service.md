---
title: Activate the Payments service
keywords:
  - activation merchant account
  - activate the payments service
---


Activating the Payments service is essential if you want to accept real credit
and debit cards as payment for your goods or services. By enabling the Payments
service, your account gains the ability to securely capture and process customer
payment information.

This means that when a customer makes a purchase, their credit or debit card
details are transmitted through our secure servers and verified by the issuing
bank. Once the payment is authorized, the funds are transferred to your account,
allowing you to complete the transaction.

Here's what activating the Payments service enables:

- Accept payments from customers using major credit and debit cards. This allows you to offer a wider range of payment options and potentially increase your sales.
- Process payments securely. Our platform uses industry-standard security protocols to protect your customers' financial information.
- Manage your transactions easily. You'll be able to view transaction history, track payments, and manage refunds directly within your account.

!!! warning
    We cannot guarantee that your merchant account will be approved to use 4Geeks Payments.

    There are a number of factors that we consider when reviewing merchant accounts,
    including the merchant's business history, the type of products or services that
    they sell, and the merchant's credit history. We also consider the merchant's
    location and the payment methods that they wish to accept.

    We do our best to approve merchant accounts that meet our requirements, but we cannot
    guarantee that your account will be approved at this time.

To successfully activate the Payments feature, it must be supported by a legal figure
according to your country, which indicates to us that such trade has permissions from your country to do business.

Once activated, there may be associated fees for processing payments.
You can find our current fee structure in our [Terms of Service](https://4geeks.io/terms).

We currently support any of the following legal figures:

- company / legal entity
- individual / natural person
- non-profit entity
- government entity (U.S. only)



## Activation form

We will ask you extra specific questions about your business
about your business to ensure we can support your operation. Our
our legal team may ask you for additional information to validate your identity,
veracity of information, business activity or other administrative information.

To activate the Payments service go to:

1. [Login](https://console.4geeks.io) with your 4Geeks account.
2. Click on **Settings**.
3. Click on **Production** on side menu.
4. Choose the incorporation country from list and click **confirm**.
5. Follow steps in wizard form.

You must provide us with legal information about your business, and attach the
required documents proving the validity of your business in your country of incorporation.

Although the requirements vary according to the country of incorporation of your business,
below are the common requirements you will need to submit depending on the type of business.

In most cases we request the following legal information from you:

| Individuals                             |  Legal entity       
| -----------------------------------     | -------------------------------------------------
| Identity card                           |  Identity document of the legal representative               
|                                         |  Tax ID number

!!! note
    We do our best to review your application as quickly as possible (instantly in most cases) however, for some regions this
    review and validation process can take 2-5 days once all the correct documents have been received.



## Prohibited business

Due to rules of the Card Networks and banking partners, we unfortunately cannot
support certain types of business models that represent a different level of risk.
certain types of business models that represent a different level of risk for
our partners and 4Geeks Payments.

The following is a list of prohibited businesses that 4Geeks cannot support:

* controlled or prescription drugs.
* illegal/highly regulated substances, ingestibles, inhalants, injectables.
* pharmaceuticals or pharmaceuticals without approval/permits from the appropriate regulatory authority.
* perishable stores.
* unauthorized brand name replicas.
* sale of animals.
* media and software without copyright or authorization.
* stores related to violent activities.
* jailbreak equipment and software, hacking and cracking materials.
* pornography, prostitution and child abuse.
* migration services.
* very high interest credits, exchange houses, credit repair or modification, debt reduction, court fines (without government permission).
* weapons, sporting arms, ammunition and pyrotechnics.
* drugs, illegal substances and products.
* adult products and services, casinos and gambling.
* multilevel, cryptocurrencies, insurance, video games, crowdfunding and investment funds.
* financial services for savings, loans and grants.
* telemarketing.

Please note that we may preemptively suspend (temporarily or permanently)
your merchant account if we become aware that your business provided false or altered documents during the
documents during the onboarding process, if there is evidence that you did not deliver the products/services you
deliver the offered products/services to your customers in a timely manner.

If we have a suspicion that there are activities related to any type of fraud, money laundering or
fraud, money laundering or that you are in breach of our Terms and Conditions.
In such a case, future deposits will be frozen for a specified period.

When an account is temporarily suspended, and you want to reactivate it,
you will have 24 hours to show any evidence that clarifies the facts.
Otherwise, your account will be permanently disabled.
