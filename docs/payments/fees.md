

# 4Geeks Payments fees

4Geeks Payments itself doesn't cost anything to set up or use. It's kind of like
having a fancy mailbox to receive payments. However, there's a charge whenever a
customer uses a credit or debit card to pay through 4Geeks Payments. This fee
covers the cost of processing the transaction securely behind the scenes.
It's similar to how most stores pay a small percentage of each credit card purchase
to the credit card company.


| Country      | Domestic Fee           | International Fee                       |
|------------------------------|------------------------|-----------------------------------------|
| Australia    | 1.75% + $0.30          | 3.5% + $0.30                            |
| Austria      | 1.5% + €0.25           | 3.25% + €0.25                           |
| Belgium      | 1.5% + €0.25           | 3.25% + €0.25                           |
| Brazil       | 3.99% + $0.39          | + 2%                                    |
| Canada       | 2.9% + $0.30           | + 0.8%                                  |
| Denmark      | 1.5% + 1.80kr          | 3.25% + 1.80kr                          |
| Estonia      | 1.5% + €0.25           | 3.25% + €0.25                           |
| Finland      | 1.5% + €0.25           | 3.25% + €0.25                           |
| France       | 1.5% + €0.25           | 3.25% + €0.25                           |
| Germany      | 1.5% + €0.25           | 3.25% + €0.25                           |
| Greece       | 1.5% + €0.25           | 3.25% + €0.25                           |
| Hong Kong    | 3.4% + $2.35           | 3.4% + $0.30                            |
| India        | 2% for MasterCard/Visa | 3% for MasterCard/Visa <br> 3.5% for Amex <br> 4.3% for international in USD or other currency           |
| Ireland      | 1.5% + €0.25           | 3.25% + €0.25                           |
| Italy        | 1.5% + €0.25           | 3.25% + €0.25                           |
| Japan        | 3.6%                   | -                                       |
| Latvia       | 1.5% + €0.25           | 3.25% + €0.25                           |
| Lithuania    | 1.5% + €0.25           | 3.25% + €0.25                           |
| Luxembourg   | 1.5% + €0.25           | 3.25% + €0.25                           |
| Malaysia     | 3% + RM1.00            | + 1%                                    |
| Mexico       | 3.6% + $3.00           | + 0.5%                                  |
| Netherlands  | 1.5% + €0.25           | 3.25% + €0.25                           |
| New Zealand  | 2.7% + $0.30           | 3.7% + $0.30                            |
| Norway       | 2.4% + 2.00kr          | 3.25% + 2.00kr                          |
| Poland       | 1.5% + 1.00zł          | 3.25% + 1.00zł                          |
| Portugal     | 1.5% + €0.25           | 3.25% + €0.25                           |
| Singapore    | 3.4% + $0.50           | 3.4% + $0.50                            |
| Slovakia     | 1.5% + €0.25           | 3.25% + €0.25                           |
| Slovenia     | 1.5% + €0.25           | 3.25% + €0.25                           |
| Spain        | 1.5% + €0.25           | 3.25% + €0.25                           |
| Sweden       | 1.5% + 1.80kr          | 3.25% + 1.80kr                          |
| Switzerland  | 2.9% + CHF 0.30        | 3.25% + CHF 0.30                        |
| United Arab Emirates (UAE) | 2.9% + AED 1.00        | + 1%                                    |
| United Kingdom (UK) | 1.5% + 20p             | 2.5% + 20p for EU <br/> 3.25% + 20p for international  |
| United States (US) | 2.9% + $0.30   | + 1.5%                                  |




!!! note
    VAT may apply in some countries.
    Add 1% if currency conversion is required.
    For Costa Rica and Panama currency conversion fee is free.
