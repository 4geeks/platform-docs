---
title: Accepting a payment via no-code payment link
keywords:
  - payment links
  - no-code
  - no-code checkout forms
---

# Accepting a payment via no-code payment link

Some stores do not need a complex computer system to generate purchase orders,
keep track of customers and invoices.
orders, keep track of customers and invoices. Small stores, with business models where
business models where communication with their customers is carried out directly
on social networks need a much more agile collection tool, so we have created 4Geeks Links.

A payment link is a unique URL or hyperlink that leads to a secure payment page
where customers can make payments for products or services.

**4Geeks Links** allows you to generate no-code checkout forms in seconds,
and send it to your customers via email, Instagram, Telegram, WhatsApp, SMS, etc.
Your customers will be able to pay with any bank card, securely.

!!! note
    There's no additional costs to generate new payment links, and there's not limit.

Payment links are a convenient way for businesses to accept online payments without
having to set up a full e-commerce website or online store. Customers can simply
click on the link and enter their payment information to complete the transaction.
Payment links are commonly used by freelancers, small businesses, and service
providers who want to offer an easy way for their customers to pay for their
products or services.


If the merchant has previously uploaded a logo in [Settings](https://console.4geeks.io/settings),
then it will be automatically displayed on the checkout form.


## Generate a new payment link

Payment links includes a stronger security layer called
3D-Secure (3DS) authentication, which is an additional security layer used to
protect online transactions from fraud. 3DS is a protocol used by card networks
like Visa, Mastercard, and American Express to verify the identity of the cardholder
before authorizing a transaction.

When a payment link includes 3DS, the customer is redirected to a separate page
where they must enter a one-time password or answer security questions to confirm
their identity. Once the authentication process is completed, the payment can be processed.

By including 3DS in payment links, businesses can reduce the risk of fraud and
chargebacks, while also providing their customers with a secure and trustworthy
payment experience.

### 1. Select or create a product

Payment links (2nd generation) allows you to attach a product, previously created
from [Catalog](https://console.4geeks.io/catalog), to charge your customer.
You can create a product only once and attach it to multiple payment links.
A product may contain a name, description, photo, amount,
tax, and some other attributes.

But if you haven't previously created a product, don't worry, because you can
also create it when you generate the payment link.

You will be able to generate a payment link in any [available currencies](/billing/features/currency#currencies-available).

To create or select a product please follow the next steps:

- In the Console, click on **Sales** -> **Payment Link (2nd gen)**.
- Click on **New Link**.
- Then select the product (if exist) or create a new one right here by filling out name, price and currency.

Set a recurrence if you want to charge the same product to the same customer in many times in future (weekly, monthly, annual, etc) automatically, or set **one-time** if you only want to charge once.

![create payment link](../../img/payments/new-payment-link.png "create payment link")

Payment links will expire in 7 days. Once expired, it can no longer be used, so you must create a new payment link.


### 2. Send the payment link

4Geeks can send the payment link to your customer by email. So click on
**Create and send by mail**. Then you need to input your customer's name and email.

If you chose 4Geeks to send the payment link and your customer claims that he
does not receive any email, please ask your customer to check the spam folder.

![send payment link](../../img/payments/send-payment-link.png "send payment link")


Or click on **Create now** if you only want to copy
the URL and send it to your customer by your own way (Instagram, SMS, WhatsApp, etc).

![url payment link](../../img/payments/url-payment-link.png "url payment link")
