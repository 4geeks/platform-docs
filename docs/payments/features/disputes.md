---
title: Chargebacks and banking disputes
description: Everything on how to counter and prevent chargebacks.
---

# Chargebacks and banking disputes

A bank dispute on online payments, also known as a chargeback, is a process in
which a consumer requests a refund of a payment made through their bank or credit
card company. This may occur when the consumer believes that they have been wrongly
charged for a purchase or that the merchant did not fulfill their obligations as
outlined in the purchase agreement.

The dispute process typically involves the consumer submitting a claim to their
bank or credit card company, outlining the details of the disputed transaction
and providing any relevant documentation, such as receipts or emails. The bank or
credit card company then reviews the claim and may reach out to the merchant for
additional information or to attempt to resolve the dispute directly.

If the bank or credit card company finds that the dispute is valid, they may
issue a refund to the consumer and then attempt to recover the funds from the
merchant. In some cases, the bank or credit card company may also assess a
chargeback fee to the merchant as a penalty for the dispute.

Bank disputes on online payments can be a significant concern for merchants,
as they can result in lost revenue, additional fees, and damage to their
reputation. As such, it is important for merchants to have clear policies and
procedures in place for handling disputes, as well as to maintain open and
transparent communication with their customers to help prevent disputes from
occurring in the first place.


!!! warning
    4Geeks will not analyze the uploaded documentation, nor is 4Geeks responsible
    for rendering a verdict.

At the time of being notified about the dispute, the merchant has a period of time
**not more than 5 business days**, counted from the notification made by e-mail.
In the event that you exceed the aforementioned period without having uploaded documents, you will lose the right to dispute.

In the event that you provide the necessary information to dispute, the issuing bank will begin a review period
bank will begin a review period which follows its internal policies and timelines.
policies. As long as you have provided us with all the complete elements to dispute, we will handle on your behalf
to dispute, we will handle the Dispute on your behalf and with the issuing bank,
the Dispute. We are not responsible for the timing of the resolution of the dispute, or for the
of such dispute, or for the final verdict.

The total amount of the disputed transaction plus the bank's operating cost ($15 USD)
will be frozen until a resolution is received from the issuing bank.
($15 USD) will be frozen until a resolution is received from the issuing bank.

At the end of the aforementioned review period, the issuing bank shall notify of the resolution.
the resolution. In case of loss, the frozen amount corresponding to the amount in dispute plus the bank's
amount in dispute plus the bank's operating costs will be returned to your client.
client. If the dispute fails in your favor, the disputed amount will be returned to your balance;
either of these two resolutions will be notified to you via email.

## Upload evidence to counter dispute

!!! info
    Only applies if using 4Geeks Payment as payment gatewat.

It's highly recommended to upload evidence or documentation about the disputed transaction. If
you choose not to upload anything, then the issuing bank will automatically rule against you.

You will be able to upload documentation of an open dispute directly on
[Console](https://console.4geeks.io), by following the steps below:

* Click on Transactions -> [Disputes](https://console.4geeks.io/disputes), in main menu.
* Enter the open dispute you wish to upload documentation.
* At the bottom of the page you will see the space for uploading documents.


![Upload dispute files](../../img/payments/upload-dispute-files.png "Dispute files")

## Bank reconciliations

It is likely that at some point the card issuing bank will block a transaction,
and furthermore, due to each bank's internal regulatory laws, that the amount
will be withheld or frozen.

This does not represent an error on the part of 4Geeks Payments or the merchant,
but is considered an extraordinary response from the bank to a transaction that
is suspicious or could not be validated.

For this, the cardholder must contact the bank and request that the money be released.
Depending on the bank, this can take from 7 days to 1 month to be resolved when
requested. In some instances, banks return all frozen amounts once a month automatically.

Make sure the transaction has not been processed. You can request documents or
screenshots from your client, or you can check directly in the Administration Panel.
