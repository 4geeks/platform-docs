---
title: Refunds
slug: refunds
image: https://cc-og-image.vercel.app/Reembolsos.png?theme=dark&md=1&fontFamily=source-sans-pro&fontSize=125px&images=https://storage.googleapis.com/mesmerizing-matrix-1380/4geeks-docs-logo.svg
sidebar_label: Refunds
---

A refund is the return of a transaction that was previously processed satisfactorily.
satisfactorily processed. It is the most common form of reconciliation between a seller and a buyer, before the case becomes a [bank dispute](/billing/features/disputes).

The process of refunding a transaction is free, and currently you can only make a refund for the full amount of the transaction.
refund for the full amount of the transaction. If you need to make partial refunds, you will need to find alternative
find alternative channels such as a bank transfer, which you must initiate on your own
your own means in a negotiation with the buyer.

!!! note
    We recommend initiating a refund (before it becomes a dispute) as soon as possible
    when any of the following scenarios occur:

    - You were unable to verify the identity of the buyer.
    - You have problems related to the shipping address. Whether the shipping address
    is not clear or represents access difficulties for delivery.
    - The product is out of stock, so you will not be able to deliver it in a timely manner.
    - Due to general factors, you will not be able to comply with the sales agreement.


When a refund process is successfully initiated, 4Geeks will initiate the process
to return the money back to the card the money automatically back to the card
issuing bank, and it is the bank that is responsible for crediting the
responsible for timely crediting the money to the purchaser's bank account.
This process can take 5 to 10 days for the money to be reflected in the buyer's bank account.


## Refund a charge

You can initiate a transaction from the API or from the [Console](https://console.4geeks.io).
The following describes the process in each path.

!!! warning
    Once a refund is initiated, it cannot be cancelled or reversed.


### From web Console
If you want to initiate a refund from the Console, you must:

1. In the Console, go to the [transaction list](https://console.4geeks.io/transactions) and find the transaction you want to reverse.
2. Enter the details of the transaction in question.
3. Click on the **Refund** button.
![start a refund from the console](../../img/payments/start-refund.png)
4. Confirm you want to refund the full amount, or type a different amount.
5. Select the closest reason and press **Accept** to initiate the refund process.
![start a refund from the console](../../img/payments/confirm-refund.png)


### From API
Please refer to the endpoint API reference [Refunds](/api/refunds).
