---
title: Charges
slug: charges
image: https://cc-og-image.vercel.app/Charges.png?theme=dark&md=1&fontFamily=source-sans-pro&fontSize=125px&images=https://storage.googleapis.com/mesmerizing-matrix-1380/4geeks-docs-logo.svg
keywords:
  - charges
  - transaction
  - sales
---

A charge (or payment) refers to the amount of money that is deducted from a customer's payment
method (such as a credit card) in order to complete a transaction. When a customer
makes a purchase through a 4Geeks, we charges the customer's payment method for the
amount of the purchase.

Charges are typically processed in real-time, meaning that 4Geeks communicates
with the customer's payment method provider (such as a bank or credit card company)
to ensure that there are sufficient funds available to cover the charge.
Once the charge is approved, 4Geeks transfers the funds to the merchant's account,
minus any service fees for processing the transaction.

![charges](../../../img/payments/charges.png "charges")

By clicking on a charge, you can see more details of the charge, including
payment source, product and customer, and all other relevant data linked to that charge.

![charges detail](../../../img/payments/charges-detail.png "charges detail")

## Understanding charges status

There are some main statuses of a charge, which indicate your current point in a customer's buying process.

* `succeeded`: a successful charge. Amount (less fees) will be placed into your balanace.
* `processing`: the customer did not complete the authorization process yet.
* `cancelled`: the customer cancelled the authorization process.
* `failed`: the cardholder's bank denied the charge.
* `blocked`: a high-risk charge blocked by 4Geeks.

## Charges with 3D-Secure

3DS stands for 3D-Secure, which is a security protocol used by many banks and credit card companies to help prevent fraud during online transactions.

When a customer makes a payment using a 3DS-enabled card, they are redirected to the issuing bank's website to verify their identity using a password or other form of authentication. This helps to ensure that the person making the payment is the rightful owner of the card, and not a fraudster using stolen card details.

4Geeks Payments is a payment gateway that provides 3DS authentication as an additional layer of security to ensure that online payments made through their platform are secure and fraud-free. When a customer makes a payment through the 4Geeks Payments gateway, the transaction is verified using 3DS technology, which helps to reduce the risk of fraudulent transactions and chargebacks. This not only provides peace of mind for merchants, but also helps to increase customer confidence in making online purchases.

Current version is 3D-Secure 2.

!!! info "Disputed 3DS charges"
    Payments that have been successfully authenticated using 3D Secure are covered by a liability shift. Should a 3D Secure payment be disputed as fraudulent by the cardholder, the liability shifts from you to the card issuer. These types of disputes are handled internally, don’t appear in the Dashboard, and don’t result in funds being withdrawn from your 4Geeks account.

    If a customer disputes a payment for any other reason (for example, product not received), then the standard dispute process applies. As such, you should make the appropriate decisions regarding your business and how you manage disputes if they occur, and how to avoid them completely.
