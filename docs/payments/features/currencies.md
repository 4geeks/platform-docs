---
title: Currencies
keywords:
  - currencies
---

4Geeks is able to process transactions in multiple currencies automatically by using the
automatically using the currency code of the currency you want to charge.
Your customers will be charged in the currency you chose for that transaction.

You can perfectly process transactions in different currencies. We will do the
the calculations and logistics necessary to deposit that money into your bank account.
For example: charge a book for $10 USD, charge a table for 10 000 CRC.

For some cardholders, it is possible that their issuing bank may overwrite the currency in which a transaction will be charged based on their own
that a transaction will be charged based on proprietary negotiations between the cardholder and their bank.
This generally occurs with credit cards under specific conditions.
If a customer reports that this occurs and is in doubt, you should direct them to their bank.

4Geeks' responsibility is to send a card charge request to the issuing bank in the currency in which that product or service is sold.
to the issuing bank in the currency in which that product or service is sold, but unfortunately we cannot
unfortunately we cannot predict the behavior of some issuing banks.

The available currencies in which you can charge an amount are:

- USD
- AED
- AFN*
- ALL
- AMD
- ANG
- AOA*
- ARS*
- AUD
- AWG
- AZN
- BAM
- BBD
- BDT
- BGN
- BMD
- BND
- BOB*
- BRL*
- BSD
- BWP
- BYN
- BZD
- CAD
- CDF
- CHF
- CNY
- COP*
- CRC*
- CVE*
- CZK
- DKK
- DOP
- DZD
- EGP
- ETB
- EUR
- FJD
- FKP*
- GBP
- GEL
- GIP
- GMD
- GTQ*
- GYD
- HKD
- HNL*
- HRK
- HTG
- HUF
- IDR
- ILS
- INR*
- ISK
- JMD
- KES
- KGS
- KHR
- KYD
- KZT
- LAK*
- LBP
- LKR
- LRD
- LSL
- MAD
- MDL
- MKD
- MMK
- MNT
- MOP
- MRO
- MUR*
- MVR
- MWK
- MXN
- MYR
- MZN
- NAD
- NGN
- NIO*
- NOK
- NPR
- NZD
- PAB*
- PEN*
- PGK
- PHP
- PKR
- PLN
- QAR
- RON
- RSD
- RUB
- SAR
- SBD
- SCR
- SEK
- SGD
- SHP*
- SLL
- SOS
- SRD*
- STD*
- SZL
- THB
- TJS
- TOP
- TRY
- TTD
- TWD
- TZS
- UAH
- UYU*
- UZS
- WST
- XCD
- YER
- ZAR
- ZMW

!!! warning
    Currencies marked with `*` cannot be processed by American Express.

## Currency conversion

Although 4Geeks processes charges in [multiple currencies](../features/currencies.md),
there are some scenarios where a charge will be converted to the default currency.
some scenarios where a charge will be converted to the merchant's default currency.
currency of the merchant. As a reference, we use a standard conversion rate, which is based on various international
is based on various international indices and banks that are updated daily.

If the currency of a collection is different from the currency in which you receive deposits,
then a conversion will occur.

In order for you to make your own estimates and calculations,
you can consult the [estimated reference value](https://api.4geeks.io/manager/countries/),
where **1 USD** equals each value per currency. These values are refreshed one or two times a day.

It is important to mention that 4Geeks does not add any additional fees to the
referral conversion rate used. conversion rate used.
