---
title: Fraud prevention
image: https://cc-og-image.vercel.app/Prevención de fraudes.png?theme=dark&md=1&fontFamily=source-sans-pro&fontSize=125px&images=https://storage.googleapis.com/mesmerizing-matrix-1380/4geeks-docs-logo.svg
keywords:
  - 4geeks fraud prevention
---

When it comes to online payments, security is definitely a very important issue that you need to pay attention to
you should pay attention to, so you know how to recognize what the risks are and how to minimize them.
and how to minimize them.

In this task you are not alone, because 4Geeks Payments, by default, puts in place transparent fraud prevention systems and automatically responds to each case.
transparent fraud prevention systems and automatically responds to every case,
on every request. In addition, we make recommendations on different methods that you can
implement to reduce the number of fraudulent transactions at your merchant.

## Card code

For Visa and MasterCard cards, CVC is a 3-digit number printed on the back of the card. For American Express cards, the CVC is printed on the front side.

All transactions at 4Geeks Payments are verified with the `CVC` security code, and this
security code `CVC`, and this guarantees us greater control over the cards used for a transaction.

![CVC](../../img/payments/credit-card-cvv.png)

CVCs add another layer of protection against identity theft and can help prevent unauthorized transactions. While many major retailers store your credit card account number in their databases, your CVC is not allowed to be stored after the card is authorized due to credit card compliance standards.

This means that even if identity thieves hack into a merchant's system and steal your credit card number, or otherwise gain access to your credit card number, they may not be able to use your card information if they don't have the code when they attempt an online connection or phone purchase.

Please note that businesses are not currently required to request a CVC code, and not all do. Also, some retailers will ask for it the first time you make a purchase to verify your identity, but then will not require it on subsequent purchases if you are logged into their website as a customer.

It's also possible for identity thieves to use malicious software known as malware to steal your CVC codes from retailers, or thieves could get one from you in a phishing attempt if you're not careful. Also, if someone steals your physical card, they will have access to it. Some financial institutions are experimenting with dynamic CVCs, or CVCs that change periodically, to make it even more difficult for thieves to make fraudulent purchases.


## Risk detection
4Geeks Payments has anti-fraud technology that studies all transactions made, and makes decisions according to different cardholder or card
and makes decisions according to different cardholder or card patterns.
