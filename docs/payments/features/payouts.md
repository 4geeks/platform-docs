---
title: Receive payouts
image: https://cc-og-image.vercel.app/Balance and payouts.png?theme=dark&md=1&fontFamily=source-sans-pro&fontSize=125px&images=https://storage.googleapis.com/mesmerizing-matrix-1380/4geeks-docs-logo.svg
keywords:
  - payouts
  - balance
---

A payout represents the money that 4Geeks collects from your sales, and sends
it periodically to the merchant's bank account.

In Console -> [Payouts](https://console.4geeks.io/payouts) you can keep a
detailed control of each deposit made or pending. Each line of the table
represents a different deposit. Generally, each deposit groups one or several
successful transactions that your business has made during that period.

Although we try to predict when the money will arrive in your bank account,
keep in mind that, depending on your bank and country, you may incur flow arrears.
When you detect a delay in a payment, please check the status on Console.

We strive every day to ensure that there are no arrears, and you can receive
payments in a timely manner. However, there are intermediary entities, which
makes it impossible for us to predict accreditation times.


By default, all confirmed sales will be added to the **pending balance** sheet
and will remain there for 5 calendar days, which works as a type of backup in
case any transaction is not recognized by its buyer, enters into dispute or
changes its risk ranking. After 5 days, the amount will become part of the
available balance.

The **available balance** is money in your favor that will be paid on the next
enabled date.

!!! note
    By default 4Geeks Payment Gateway generates a new payout every week.

When a deposit fails, for any reason, it can take up to 5 days to update your
status from **paid** to **failed**. We will do our best to show you the reason. Future
deposits are automatically paused until you manage to correct the inconvenience
with your bank, or send us a new bank account.

## Payout currency
As a merchant, you can accept payments in multiple currencies with the same
integration, without the need for additional processes. However, we can only
send deposits in specific currencies depending on the country where your business
is legally based. Then 4Geeks will apply a standard conversion rate.

The following table mentions the currencies in which we will make deposits
according to the country of incorporation of trade:



| Country            | Deposit currency     
| -----------------  | ---------------------
| United Kingdom     | EUR, GBP
| Spain              | EUR
| Canada             | CAD, USD
| United States      | USD
| Mexico             | MXN
| Guatemala          | GTQ
| El Salvador        | USD
| Costa Rica         | CRC
| Panama             | USD
| Colombia           | COP
| Bolivia            | BOB
| Paraguay           | PYG
| Uruguay            | UYU
| Peru               | PEN
| Argentina          | ARS


For example:
- A Costa Rica-based merchant that charge in `USD`, will get money in `CRC` in the bank account.
- Un comercio basado en España que cobre a sus clientes `EUR`, recibirá depósitos en `EUR`.
- Un comercio basado en Panamá que cobre a sus clientes en `USD`, `CRC` y `EUR`, recibirá depósitos en `USD`.


## Price per payout
4Geeks Payment Gateway does not charge any commission for initiate a payout. However, depending
on your country, bank and deposit method, you may suffer additional deductions
for transportation or crediting the money, so if you see less amount of money
credited, you'll have to contact your bank.

## Tracking a payout
To make it easier to track a deposit to your bank account, we automatically add
labels with the current status of that deposit. There is a way to go between a
deposit leaving 4Geeks until it reaches your bank account.

Below we explain the meaning of each state:

* __Pending__: These are all deposits that have not yet been paid, but that are
already queued to be paid.
* __In transit__: It means that the deposit has already left 4Geeks, and is in
the process of validation or accreditation by intermediary banks or your bank.
It usually takes 1-3 days for a deposit to be credited.
* __Paid__: Deposits that have already been satisfactorily credited by your bank.
Some banks needs 1 more additional day to update your bank account balance.
* __Failed__: A deposit that has not been credited by the bank and has been returned.
Failed deposits can be queued to be paid on the next available date.
* __Cancelled__: A interrupted deposit by 4Geeks, the bank or payment gateway.
