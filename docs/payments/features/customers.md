---
title: Customers
image: https://cc-og-image.vercel.app/Customers.png?theme=dark&md=1&fontFamily=source-sans-pro&fontSize=125px&images=https://storage.googleapis.com/mesmerizing-matrix-1380/4geeks-docs-logo.svg
sidebar_label: Customers
keywords:
  - customers
---

The Customers category in the 4Geeks Payments dashboard allows you to view and
manage all of your customers. Every customer will show their full name and email
address. Customers are created automatically per new sale, whether it is made
through a plugin, API, or payment link.


The following information is displayed for each customer:

* Full name
* Email address
* Creation date
* Last updated date
* Total sales
* Number of payments
* Last payment date
* Payment method

The Customers category is a valuable resource for businesses of all sizes.
It allows you to track your customer base, identify potential leads, and provide
excellent customer service.
