---
title: Currency conversion
keywords:
  - currencies
---

!!! note
    Only applies if using 4Geeks Payments as payment gateway in production environment.

Although 4Geeks processes charges in [multiple currencies](../features/currencies.md),
there are some scenarios where a charge will be converted to the default currency.
some scenarios where a charge will be converted to the merchant's default currency.
currency of the merchant. As a reference, we use a standard conversion rate, which is based on various international
is based on various international indices and banks that are updated daily.

If the currency of a collection is different from the currency in which you receive deposits,
then a conversion will occur.

In order for you to make your own estimates and calculations,
you can consult the [estimated reference value](https://api.4geeks.io/manager/countries/),
where **1 USD** equals each value per currency. These values are refreshed one or two times a day.

It is important to mention that 4Geeks does not add any additional fees to the
referral conversion rate used. conversion rate used.
