---
title: 4Geeks Payments overview
keywords:
  - 4geeks payments
  - payment gateway
  - accept online payments
  - online payments
---


Empower your business with 4Geeks Payments, a comprehensive revenue growth platform
designed to simplify and enhance subscription management for both offline and online
businesses. Our robust platform streamlines the entire subscription lifecycle,
from customer onboarding to recurring billing and payment processing, enabling
you to focus on fostering customer relationships and expanding your business.

4Geeks Payments' integration with payment gateways streamlines the payment
process, ensuring a smooth and secure experience for both your business and your customers.

This guide will allow you to understand how 4Geeks Payments works, the API,
as well as explanations and integration examples.

4Geeks Payments is the ideal solution for businesses of all sizes seeking to:

* Simplify and streamline subscription management
* Automate billing and payment processes
* Enhance customer experience and satisfaction
* Gain real-time insights into subscription performance
* Drive revenue growth and business expansion

## Payment gateway incluided

4Geeks Payments takes the hassle out of accepting online payments. Unlike other solutions that require separate gateway integration, **4Geeks Payments includes a secure and reliable payment gateway** pre-activated within your merchant account. This means you can start accepting payments immediately, without any complex setup procedures.

Furthermore, 4Geeks Payments boasts competitive [fees](fees.md), ensuring you keep more of your hard-earned revenue. Plus, they offer local payouts, simplifying your financial management and streamlining the flow of funds into your account. This combination of ease of use, affordability, and convenient payouts makes 4Geeks Payments the perfect choice for businesses of all sizes looking to streamline their online payment processing.

To understand how we process payments for your business, follow this steps:

1. When a customer initiates a payment, 4Geeks Payments securely transmits the
payment details to the payment gateway.
2. The payment gateway verifies the customer's payment information and communicates
with the issuing bank or financial institution.
3. Upon successful authorization, the payment gateway processes the transaction
and sends a confirmation back to 4Geeks Payments.
4. Then 4Geeks Payments records the payment and updates the customer's account accordingly.


## Start accepting online payments

In order to accept online payments you need an [activated Payments service](activate-service.md) account.
4Geeks Payments provides multiple ways to integrate to mobile apps, websites and platforms:

- no-code tools
- payments on your website

### No-code tools
It is relatively easy to accept online payments via no-code tools. There are
several no-code platforms that offer built-in payment processing capabilities,
such as Squarespace, Shopify, Wix, and Weebly. These platforms allow users to
create online stores or websites and accept payments without the need for extensive
technical knowledge or coding skills.

We've made it easy to accept payments without the hassle of developing complex
transactional systems. You don't need technical knowledge or expensive equipment
to start accepting online payments in your online store, billing software or
customer management system.

If you're looking for 4Geeks Payments integration and don't want to go through
the hassle of technical programming knowledge, then look no further than our
list of platforms. There's no limit to the type of business you have or how
long it's taken you to get started.

<div class="grid cards" markdown>

-   :material-clock-fast:{ .lg .middle } __Payment links__

    ---

    4Geeks provides no-code tools to start accepting online payments on your
    website or platform like payment links. Which means generate payment link
    and get paid on social media, WhatsApp or email.

    [:octicons-arrow-right-24: Read more about payment links](/payments/payment-links)

</div>


### Payments integrated on your website

The simplest way to integrate 4Geeks on custom websites or platforms,
is to use one of the [plugins](plugins) for any of the available
platforms such as [Wordpress/WooCommerce](/payments/plugins/woocommerce), Magento, Odoo, etc.

Alternatively we provide [third-parties client libraries](/api#client-libraries) that you can use
to integrate the API with your platform in a few lines of code. These libraries are maintained
and supported, for the most part, by the community, for languages such as Ruby
on Rails, Python, . NET and Go. However, you can always consume the API using
HTTP methods, or write your own libraries.

The following example is a **simple charge**, which does not require you to
register cards or customers.


```shell
curl 'https://api.4geeks.io/v1/charges/simple/create/' \
    -X POST \
    -H 'authorization: bearer PdSKf04xi9LEcvcwIAdbWAXVg380Kz' \
    -F 'amount=800.55' \
    -F 'description=Desc for Simple' \
    -F 'entity_description=Desc for Simple' \
    -F 'currency=usd' \
    -F 'credit_card_number=4242424242424242' \
    -F 'credit_card_security_code_number=123' \
    -F 'exp_month=11' \
    -F 'exp_year=2020'
```


Check all the available endpoints, as well as specific examples for each endpoint
in the [API reference](/api).

[Go to API Reference](/api){ .md-button }
