---
title: Install and config the WooCommerce plugin
keywords:
  - wooocommerce plugin
---

WooCommerce is a popular open-source e-commerce plugin designed to work
with WordPress, a popular content management system (CMS). It allows users
to easily create and manage online stores, selling physical and digital products,
and accepting payments via a variety of payment gateways.

WooCommerce is highly customizable and offers a wide range of features, including
product management, inventory management, tax and shipping options, customer management,
and various marketing tools. It also supports a large number of extensions and
integrations, enabling users to add additional features and functionality to
their online stores.

Here you'll find how to install and setup the plugin to integrate 4Geeks Payments
on your WordPress/WooCommerce instance.


## Install the plugin

!!! info
    Need to install the WooCommerce plugin manually? Find the source right on the official
    [Wordpress repository](https://wordpress.org/plugins/payments4g-4geeks-payments/).

I'm assuming that you already have the WooCommerce installed on your Wordpress instance.
To install the **4Geeks Payment for WooCommerce** plugin please follow the next steps:

1. Go to the login page of your WordPress website and enter your login credentials to access the dashboard.
2. In the left-hand menu of the WordPress dashboard, click on the "Plugins" option.
3. From the "Plugins" page, click on the "Add New" button at the top of the page.
4. Search for the plugin by typing "4Geeks".
5. Once you find the plugin you want to install, click on the "Install Now" button.
6. After the plugin is installed, click on the "Activate" button to activate the plugin.
![browse plugins](../../img/payments/woocommerce-plugin-add-4geeks.png "install woocommerce plugin")

## Setup the plugin
Once the plugin is installed and activated, you need to link it to your merchant
account. So follow the next steps:

1. On Wordpress, go to "WooCommerce" -> "Settings" -> "Payments".
2. Click on "4Geeks Payments" plugin from list.

![manage plugin](../../img/payments/woocommerce-manage-plugin.png "manage plugin")

3. Copy the **Client ID** and **Client Secret** from Console (Settings -> API Keys), and
paste it on the plugin configuration.
4. Click on "Save changes".
5. That's it.
![save api keys](../../img/payments/woocommerce-plugin-apikeys.png "save api keys")
