---
title: Hire an employee
image: https://cc-og-image.vercel.app/Hire an employee.png?theme=light&md=1&fontFamily=source-sans-pro&fontSize=125px&images=https://tenndo.com/assets/img/tenndo-color-logo.png
---


## Employer of Record {#eor}
An Employer of Record (EOR) is an organization (4Geeks in this case) that
serves as the employer for tax purposes while the employee performs work at a
different company.

The Employer of Record takes on the responsibility of
traditional employment tasks and liabilities.

Specifically, the employer of record is the legal entity that:

- Arranges all visas and work permits for the employee, avoiding delays or refusals
- Provides a registered entity for running a local, compliant payroll inside the country
- Meets all host country labor laws pertaining to local contracts and worker protections
- Advises the client of required notice periods, termination rules and severance pay
- Is the host country interface between the employee and government authorities
