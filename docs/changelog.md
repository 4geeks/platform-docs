---
title: Changelog
slug: /
---

## May 2024
- Payments is now available in over 30 countries.
- A new [community forum](https://community.4geeks.io) has been launched to connect with other users.

## March 2024
- Charges API deprecated in favor of [Payments API](api/endpoints/payments.md), which includes 3DS for all new payment intents by default.
- New help documentation website.
- New [customer support](support.md) packages available.

## March 2023
- (New) Products now comes with a shareable link to buy.
- (New) Checkout page.
- Partial or full refunds from the Console.
- Browse into payments or refunds included in a payout.
- Minor fixes and security improvements on WooCommerce plugin.
- Now show the conversation rate (if exist) for a transaction, on Console.
- (Deprecated) Payment links first generation are officially out of service.

## January 2023
- New! Payment link second generation, which allow to attach a product and customer.
- 3D-Secure implementation on payment links.
- Product management.
- Customer management.
- Wordpress plugin updated to collect customer more customer info.
- 3D-Secure implementation on Wordpress plugin.
- Better high-risk payment discovery and management.


## October 2022
- Console is now presented in English or Spanish.
- Sidebar navigation rearranged.
- Now it is possible to log in with the username or the email registered.
- We create a filter on the available candidates by professional category, and you can also see more information on the CVs.
- Payment Links are now shown by IDs on the table list.
- Bank Account Number can be updated on Settings.

## September 2022
- The charges graph of the Dashboard is now displayed in each user’s default currency.
- New currencies are now accepted for payments. Check the list of the new accepted countries with their respective currencies in the official documentation.
- Minor UX improvements on Console settings section.
- Plans can now be charged in multiple currencies.
- Payment Links can now be charged in multiple currencies.

## August 2022
- Publish new service available: [4Geeks Talent](https://4geeks.io/talent).
- Minor styles updates in Console.
- Addition of new currencies in payment processing, from the list of countries allowed in 4Geeks Payments.
- Able to cancel a Payment Link already created.
