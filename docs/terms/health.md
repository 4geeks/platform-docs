---
title: 4Geeks Health Terms
hide: 
    - feedback
---


This SaaS Terms and Conditions Agreement (the "Agreement") is entered into between 4Geeks Technologies, Inc., a Delaware corporation with its principal place of business at Cerventille Road Suite 400, Wilmington, Delaware 19808 ("Provider"), and the entity identified in the Order Form ("Customer"), effective as of the date specified in the Order Form ("Effective Date").

## 1. Definitions

*   **"4Geeks Health"**: The software-as-a-service solution provided by Provider for managing hospitals and clinics, as described in the Order Form.
*   **"Licensed User"**: An individual authorized by Customer to use the 4Geeks Health under this Agreement.
*   **"Order Form"**: The document specifying the details of Customer’s subscription, including the number of Licensed Users, the term, and the fees.
*   **"Confidential Information"**: Any non-public information disclosed by one party to the other, including but not limited to business plans, technical data, and customer information.

## 2. Grant of License

Provider grants Customer a non-exclusive, non-transferable, revocable license to access and use the 4Geeks Health during the Term, solely for Customer’s internal business purposes in managing hospitals and clinics, subject to this Agreement’s terms and conditions. The license is limited to the number of Licensed Users specified in the Order Form. Customer may not permit access to the 4Geeks Health by more users than the specified number of Licensed Users without Provider’s prior written consent.

## 3. Term and Renewal

This Agreement commences on the Effective Date and continues for an initial term of one month (the "Initial Term"). Thereafter, it automatically renews for successive one-month terms unless either party provides written notice of non-renewal at least 30 days prior to the end of the then-current term.

## 4. Fees and Payment

Customer shall pay Provider $99 USD per Licensed User per month, as specified in the Order Form. Fees are due in advance on the first day of each month. If Customer fails to pay by the due date, Provider may:

*   Charge interest on the overdue amount at 1.5% per month or the maximum rate permitted by law, whichever is lower;
*   Suspend Customer’s access to the 4Geeks Health until payment is received.

All fees exclude taxes, and Customer is responsible for all applicable taxes, duties, and similar charges.

## 5. Scope of Use and Restrictions

Customer shall use the 4Geeks Health solely for managing hospitals and clinics. Customer is prohibited from:

*   Using the 4Geeks Health for illegal or unauthorized purposes;
*   Attempting unauthorized access to the 4Geeks Health or related systems;
*   Reverse-engineering, decompiling, or disassembling the 4Geeks Health;
*   Interfering with or disrupting the 4Geeks Health’s integrity or performance;
*   Removing or altering proprietary notices on the 4Geeks Health.

## 6. Intellectual Property

Provider retains all right, title, and interest in the 4Geeks Health, including all intellectual property rights. Customer receives only a license to use the 4Geeks Health and acquires no ownership rights.

## 7. Confidentiality

Each party shall keep the other’s Confidential Information confidential and not disclose it to third parties, except as necessary to perform this Agreement. Each party shall use at least reasonable care to protect such information, consistent with the care it applies to its own confidential information.

## 8. Data Privacy and Security

Provider shall implement reasonable technical and organizational measures to protect Customer’s data from unauthorized access, use, or disclosure. Customer is responsible for:

*   Maintaining the confidentiality of its login credentials;
*   All activities under its account.

Customer warrants that it has the right to input data into the 4Geeks Health and that such data complies with applicable laws, including patient privacy and data protection regulations (e.g., HIPAA).

## 9. Support and Maintenance

Provider shall provide reasonable technical support during normal business hours. Scheduled maintenance may render the 4Geeks Health temporarily unavailable, and Provider will use commercially reasonable efforts to notify Customer in advance.

## 10. Service Level Agreement

Provider shall strive to maintain 4Geeks Health availability at 99.9% per month, excluding scheduled maintenance. If availability falls below 99.9% in a given month, Customer *may* be eligible for a credit against future fees, as outlined in the Order Form.

## 11. Disclaimers

EXCEPT AS EXPRESSLY STATED HEREIN, THE 4Geeks Health IS PROVIDED "AS IS." PROVIDER DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.

## 12. Limitation of Liability

PROVIDER SHALL NOT BE LIABLE FOR INDIRECT, INCIDENTAL, SPECIAL, OR CONSEQUENTIAL DAMAGES (INCLUDING LOSS OF PROFITS, DATA, OR USE) ARISING FROM THIS AGREEMENT, EVEN IF ADVISED OF SUCH POSSIBILITIES. PROVIDER’S TOTAL LIABILITY SHALL NOT EXCEED THE AMOUNT PAID BY CUSTOMER TO PROVIDER IN THE 12 MONTHS PRECEDING THE CLAIM.

## 13. Indemnification

Customer shall indemnify, defend, and hold harmless Provider from claims, liabilities, damages, losses, and expenses (including reasonable attorneys’ fees) arising from Customer’s use of the 4Geeks Health, its data, or its breach of this Agreement.

## 14. Termination

Either party may terminate this Agreement for cause if the other materially breaches it and fails to cure the breach within 30 days of written notice. Provider may terminate immediately for non-payment. Upon termination:

*   Customer’s right to use the 4Geeks Health ceases;
*   Customer shall return or destroy Provider’s materials;
*   Provider shall make Customer’s data available for download for 30 days post-termination.

## 15. Governing Law and Dispute Resolution

This Agreement is governed by Delaware law, without regard to conflict of laws principles. Disputes shall be resolved via binding arbitration under the American Arbitration Association’s rules, conducted in Delaware.

## 16. Force Majeure

Neither party is liable for performance failures due to events beyond reasonable control, such as natural disasters, war, or strikes.

## 17. Amendments

This Agreement may only be amended in writing signed by both parties.

## 18. Severability

If any provision is invalid or unenforceable, the remaining provisions remain effective.

## 19. Entire Agreement

This Agreement, with the Order Form, constitutes the entire understanding between the parties, superseding all prior agreements or communications.

## 20. Assignment

Customer may not assign or transfer this Agreement without Provider’s prior written consent. Attempted assignments without consent are void.

## 21. Notices

Notices must be in writing and sent to the addresses in the Order Form (or updated addresses provided by notice). Notices are effective upon receipt if sent by certified mail or upon delivery if by overnight courier or in person.

## 22. Modifications to the 4Geeks Health

Provider may modify the 4Geeks Health, including adding or removing features, provided such changes do not materially reduce functionality.

## 23. Feedback

Customer feedback or suggestions may be used by Provider royalty-free, worldwide, and perpetually to enhance the 4Geeks Health.

## 24. Data Backups

Provider shall maintain regular backups and reasonable disaster recovery procedures for Customer’s data. Customer is encouraged to keep its own backups.