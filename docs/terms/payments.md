---
title: 4Geeks Payments Terms
hide: 
    - feedback
---



Thanks for using our products and services ("Services"). The Services are provided by 4Geeks Technologies, Inc. ("4Geeks", "we", "us"), located at 2711 Centerville Road Wilmington, DE 19808 United States of America. 

By using our Services, you are agreeing to these terms. Please read them carefully. 

Our Services are very diverse, so sometimes additional terms or product requirements (including age requirements) may apply. Additional terms will be available with the relevant Services, and those additional terms become part of your agreement with us if you use those Services. 

## 1. Acceptance of Terms

These Terms of Use govern the services provided to you ( "Supplier", "Merchant", "Client" or "you" ) by 4Geeks (as defined below). 

By signing up for our services, or by downloading, installing or otherwise accessing or using the Services (as defined below), you agree that you have read, understand, and accept the terms and conditions described below (the “Terms of Use”) and you agree to be bound by these Terms of Use and all terms, policies and guidelines incorporated in the Terms of Use by reference (including Privacy Policy which can be found at [https://4geeks.io/privacy](https://4geeks.io/privacy) (or such other URL that 4Geeks may provide from time to time). 

If you do not agree to these Terms of Use, you do not have our permission to, and may not use the Services (as defined below) in any way. The Services are offered to you conditional on your acceptance of these Terms of Use. 

We may make changes to these Terms of Use from time to time. When we do, we will revise the “Last Updated” date given above. Modifications are effective upon publication. It is your responsibility to review these Terms of Use frequently and to remain informed of any changes to them. The then-current version of these Terms of Use will supersede all earlier versions. You agree that your continued use of our Services after such changes have been published will constitute your acceptance of such revised Terms of Use. Disputes arising under these Terms will be resolved in accordance with the version of these Terms that was in effect at the time the dispute arose. 

## 2. Services

In terms of this agreement 4Geeks will provide the following Services: 

**2.1.** Setting you up as a Merchant of the Product on 4Geeks' platform and establishing a Merchant Account which provides you with access to the 4Geeks Console and allows you to view all sales made by 4Geeks and the monies which are due to the Merchant. 

**2.2.** Acting as your non-exclusive reseller of the Product via 4Geeks' platform across all territories supported by 4Geeks from time to time during the term of these Terms of Use (for the avoidance of doubt nothing in these Terms of Use creates an obligation for 4Geeks to sell across all territories). 

**2.3.** Product fulfilment, order support and being responsible for all aspects of Sales Tax as the reseller of the Product. 

## 3. Payments, Deposits and Payouts

**3.1.** 4Geeks shall deposit to Merchant for each successful completed sale of the Product, less any sales tax, the 4Geeks commission (5% + $0.50 USD per successful transaction), disputed transactions, chargebacks and related bank fees ($15 USD per lost dispute). 

**3.2.** 4Geeks will make international transfers (or domestic transfer if possible) at no extra costs. 4Geeks is not responsible for any charges or fees associated with the making of transfers via international bank transfer. 

**3.3.** 4Geeks will report to you in USD currency, however you will be able to access original currency on transactions detail. 

**3.4.** Depending on the risk level of the merchant acount, 4Geeks can freeze money from the available balance at anytime, to deal with possible bank dispute in the future.

## 4. Merchant Obligations

The Merchant must, before entering into these Terms of Use and thereafter on our request, provide information about itself and its business which may include information about its financial status and creditworthiness, its activities, its shareholders (and ultimate beneficial owners), the Products and URLs, as we or our third party KYC verification partners request from time to time (the "Merchant Information"). The Merchant represents and warrants unconditionally that all Merchant Information it provides to us is correct and up to date and undertakes to provide us with at least five (5) Business Days' prior written notice of any material change to any of the Merchant Information. 

If the Merchant fails to provide the Merchant Information requested, we reserve the right to suspend the provision of our Services until such Merchant Information is provided. 

The Merchant shall provide correct and complete URL(s) to us. The Merchant may, subject to our prior written approval, amend its existing URL(s) or add new URL(s) from time to time, in which case the same obligations as apply to existing URLs shall apply to such amended or additional URL(s). 

The Merchant is responsible for correctly entering all information into the 4Geeks Console. 

## 5. Warranties

You represent and warrant to 4Geeks that:

**5.1.** the information you have provided on the 4Geeks Console is correct and up to date. 

**5.2.** the information you have provided in respect of the Product or type or category of Product is correct and up to date. 

**5.3.** you are the owner of each Product in connection with the use of the Services or that you are legally authorised to act on behalf of the owner of such Product for the purposes of these Terms of Use. 

**5.4.** the Product complies with our Acceptable Use Policy and the sale of the Product is in compliance with all Payment Scheme Rules and applicable laws in the countries where the Buyers are based. 

**5.5.** you own and operate the URL(s) listed in your application for a Merchant Account and/or as otherwise approved by 4Geeks from time to time. 

**5.6.** you will not use the Services to facilitate the sale of Products on websites or applications other than the URL(s). 

**5.7.** the Product is free from defect and fit for purpose.

**5.8.** you have the necessary right, power and authority to enter into these Terms of Use and to perform the acts required of you hereunder and to permit 4Geeks to perform the Services contemplated under these Terms of Use. 

**5.9.** your use of the Services and the delivery and performance by you of the terms and conditions under these Terms of Use do not and will not conflict with or violate any agreement or other instrument with a third party applicable to you or otherwise infringe upon the rights of any third party. 

**5.10.** you have complied and will continue to comply with all applicable laws, statutes, ordinances and regulations (including, without limitation, the Data Protection Legislation in respect of any Buyer data passed to you by 4Geeks). 

**5.11.** you will at all times comply with all applicable 4Geeks policies. 

You agree to indemnify, defend and hold harmless 4Geeks, its employees, officers and directors, or users from and against any and all claims, liabilities, penalties, settlements, judgments, fees (including reasonable legal fees) arising from: 

any information that you or anyone using your account may submit or access in the course of using the Services. 
your breach of any representation or warranty in, or violation of the terms of these Terms of Use or any agreement or other instrument with a third party applicable to you. 
any violation or failure by you to comply with all laws and regulations in connection with your use of the Services, whether or not described herein. 
any disputes in respect of the Product.

## 6. Confidentiality

All Confidential Information provided by one party to any other party under these Terms of Use is deemed to be confidential. The receiving party shall not use, disclose, or otherwise take any advantage of such Confidential Information. In particular: 

**6.1** each party shall exercise the same degree of care to avoid the publication or dissemination of the confidential information of the other party as it affords to its own confidential information of a similar nature which it desires not to be published or disseminated, which in any event shall not be less than reasonable care. 

**6.2** Confidential Information disclosed under these Terms of Use shall only be used by the receiving party within the purpose of these Terms of Use or the performance of its obligations hereunder. The receiving party agrees not to use the disclosing party’s confidential information except in the course of performing hereunder and will not use such confidential information for its own benefit or for the benefit of any third party. 

**6.3** the obligation of the parties not to disclose confidential information shall survive the termination or cancellation of these Terms of Use. However, no party shall be obligated to protect confidential information of the other party which: 

is rightfully received by the receiving party from another party without confidential obligation to such party, or 
is known to or developed by the receiving party independently without use of the confidential information, or 
is, or becomes generally known to the public by other than a breach of duty hereunder by the receiving party; and 
furthermore, a receiving party may disclose confidential information that is required to be disclosed pursuant to a requirement of a government agency or law so long as the receiving party provides prompt notice to the disclosing party of such requirement prior to disclosure. 

## 7. Suspended Accounts

**7.1.** Where you have no sales for a period of six (6) consecutive months and there is a positive Account Balance, 4Geeks reserves the right to charge you an account dormancy charge and/or deactivate your Supplier Account. No-activity accounts with a negative balance and no sales activity in the preceding 15 days will be deactivated immediately. 

**7.2.** Without prejudice to any other rights or remedies we may have, the Supplier hereby authorises us to set-off by whatever means the whole or any part of the Supplier's liability to us under these Terms of Use against any funds, sums or other amounts owing to, the Supplier under these Terms of Use including but not limited to: (i) liability for refunds and Chargebacks; (ii) any fines issued for non compliance with the Payment Scheme Rules; (iii) business listed in Prohibited Businesses ([https://docs.4geeks.io/payments/prohibited-businesses](https://docs.4geeks.io/payments/prohibited-businesses)); (iv) fraudulent or illegal use of our Services; or (v) put 4Geeks reputation at risk. 

**7.3.** If there are insufficient funds (less than a sum equalling your refund rate or chargeback rate as applicable x gross sales in the previous 90 days) at any time to cover potential refunds, chargebacks, charges against the Supplier Account or other liabilities you may owe to us ("Liabilities"), you agree to either put 4Geeks in sufficient funds or agree that we may exercise the right of set-off in clause 7.1 at any time without notice to the Supplier whether such liability is present or future, liquidated or unliquidated, actual or contingent. If the liability to be set off is expressed in different currencies, we may convert such liability at a market rate of exchange for the purpose of set-off. In the event such set-off does not fully reimburse us for the liability owed, the Supplier shall immediately pay us a sum equal to any shortfall. 

**7.4.** For the avoidance of doubt, 4Geeks is not obliged to pay any revenues associated with activities or Products which it considers in its discretion to be fraudulent or illegal under any relevant law or regulation. 

## 8. Rights and Obligations on Termination or Expiration

**8.1.** Termination or expiration of these Terms of Use shall not release either party from the obligation to make payment of all amounts then or thereafter due and payable. 

**8.2.** Upon termination or expiration of these Terms of Use, 4Geeks will within thirty (30) days return to you or destroy (i) all access details to the Product; (ii) all manuals, documentation, product literature, fee schedules and other written materials provided by you; or (iii) all Confidential Information and other property of you, provided that such materials or information are in its possession or under its control. 

## 9. General

**9.1.** The parties agree that these Terms of Use constitute the entire agreement between them and supersedes all previous agreements, understandings and arrangements between them, whether in writing or oral in respect of its subject matter. 

**9.2.** Each party acknowledges that it has not entered into these Terms of Use in reliance on, and shall have no remedies in respect of, any representation or warranty that is not expressly set out in these Terms of Use. No party shall have any claim for innocent or negligent misrepresentation on the basis of any statement in these Terms of Use. 

**9.3.** You may not assign, subcontract or encumber any right or obligation under these Terms of Use, in whole or in part, without 4Geeks's prior written consent, such consent not to be unreasonably withheld or delayed. Any assignment in violation of this Section 20.3 is void. 

**9.4.** A party shall not be in breach of these Terms of Use nor liable for delay in performing, or failure to perform, any of its obligations under these Terms of Use if such delay or failure result from an event, circumstance or cause beyond a party’s reasonable control. In such circumstances the affected party shall be entitled to a reasonable extension of the time for performing such obligations. 

**9.5.** Unless otherwise expressly agreed, no delay, act or omission by either party in exercising any right or remedy will be deemed a waiver of that, or any other, right or remedy. 

**9.6.** Except as expressly provided in these Terms of Use, the rights and remedies provided under these Terms of Use are in addition to, and not exclusive of, any rights or remedies provided by law. 

**9.7.** If any provision or part-provision of these Terms of Use is or becomes invalid, illegal or unenforceable, it shall be deemed modified to the minimum extent necessary to make it valid, legal and enforceable. If such modification is not possible, the relevant provision or part-provision shall be deemed deleted. Any modification to or deletion of a provision or part-provision under this clause shall not affect the validity and enforceability of the rest of these Terms of Use. 

**9.8.** Each party shall comply with all laws, enactments, regulations, regulatory policies, guidelines and industry codes applicable to it and shall maintain such authorisations and all other approvals, permits and authorities as are required from time to time to perform its obligations under or in connection with these Terms of Use. 

**9.9.** The parties are independent businesses and not partners, principal and agent, or employer and employee, or in any other relationship of trust to each other. 

**9.10.** Any notice (save for notices given in legal proceedings or arbitration) given to a party under or in connection with these Terms of Use shall be in writing or by email (support@4geeks.io) to the address given for the relevant party herein or such other address that a party notifies the other party of at any time and shall be given and deemed received by first class post on the second Business Day after postage or, if given by hand on delivery. 

**9.11.** 4Geeks could use Supplier name, logo, social networks profiles and any other public information for marketing matters. 

## 10. Governing Law and Jurisdiction

These Terms of Use shall be governed by and construed in accordance with the law of United States and the parties irrevocably agree that the United States courts shall have exclusive jurisdiction over any claim or matter arising under or in connection with these Terms of Use.