---
title: Create a 4Geeks account
---

# Create a 4Geeks account

To start using any 4Geeks product to must to create a 4Geeks account.
The process is quick and easy, and you can
get started in just a few minutes.

!!! note
    Does not applies for 4Geeks Perks.

![create account](../../img/billing/create-account.png "create 4geeks account")

1. Open your web browser and navigate to the [4Geeks Console](https://console.4geeks.io/signup).
2. On the account creation page, enter your personal and business information.
3. Review and agree to the terms and conditions.
4. Click the "Create Account" button.

Once completed you'll receive a confirmation email.

!!! note
    * Email address will serve as your primary login credential for accessing your 4Geeks account.
    * Use a strong password that is at least 8 characters long.
    * Use a business email address that you check regularly.
    * Make sure your company name is spelled correctly.


Check all the available endpoints, as well as specific examples for each endpoint
in the [API reference](/api).

[Go to API Reference](/api){ .md-button }
