---
title: Authentication
image: https://cc-og-image.vercel.app/Authentication API.png?theme=dark&md=1&fontFamily=source-sans-pro&fontSize=125px&images=https://storage.googleapis.com/mesmerizing-matrix-1380/4geeks-docs-logo.svg
slug: /authentication
---

!!! info "Endpoint"
    `POST /authentication/token/`

4Geeks uses [oAuth2](https://en.wikipedia.org/wiki/OAuth#OAuth_2.0) as the authentication standard to use the API securely. So, for each request, you must send an ephemeral token, which you generate by sending the `grant_type`, `client_secret`, and `client_credentials` values to the token generation endpoint.

This will return an `access_token`, which you need to send on every request from now on.

Remember to always keep these credentials private, and never share them with third parties. It is advisable to store these credentials in environment variables.

!!! note
    The `grant_type` parameter should always be set to `client_credentials` as shown in the example below.


=== "shell"
```shell
curl https://api.4geeks.io/authentication/token/ \
  -X POST \
  -d '{
        "grant_type": "client_credentials",
        "client_id":"kGnxskOMNedrqj8IVma23Qk2DQGpjjpAUUPLr85W",
        "client_secret":"VbCaLax7xnhsBIfmkwic6ifSZ4FRiMRfwrrtdSKhw89f17LRdyeW7zYYKLwvj2VLRbQzH6q0F7mRejGJwBIckHa7W2PlJNN4QLqQcnCMYQkYj1DK4uUv0MVCbYhVeyTX"
      }'
```


Response:

```json
{
    "access_token": "PdSKf04xi9LEcvcwIAdbWAXVg380Kz",
    "token_type": "Bearer",
    "expires_in": 36000,
    "scope": "read write groups"
}
```
