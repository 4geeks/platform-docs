---
title: API Reference
image: https://cc-og-image.vercel.app/4Geeks Payments API.png?theme=dark&md=1&fontFamily=source-sans-pro&fontSize=125px&images=https://storage.googleapis.com/mesmerizing-matrix-1380/4geeks-docs-logo.svg
slug: /
---

Through multiple endpoints it is possible to integrate some functions of
the products that 4Geeks has available for you, and they are detailed in this
documentation. Also included are examples and use cases, external references.

Most of the endpoints of this API are consumed from the backend, and with very
high security protocols. However, we always encourage developers to implement
their own security measures that strengthen and improve the experience of using
your tool.

!!! info "Base URL"
    `https://api.4geeks.io`


## API keys
Find your API keys in `Settings -> API Keys`.

![api keys](/img/payments/apikeys.png "api keys")

## Client libraries

You can also use client libraries in multiple languages:

- [NodeJS](https://github.com/cayasso/gpayments)
- [Python](https://github.com/4GeeksDev/gpayments-python)
- [Go](https://gitlab.com/esavara/gpayments)
- [Ruby](https://github.com/4GeeksDev/gpayments-ruby)
- [.NET C#](https://github.com/djhvscf/gpayments-dotnet)
- [Flutter](https://pub.dev/packages/gpayments).

### Not a developer?

Try out the 4Geeks [no-code tools](/payments/#no-code-tools),
a plugin, get help from our [support](/support) offering.


## Sandbox
This is so that each developer can ensure that the business flow runs as expected by performing the
runs as expected by performing the necessary number of simulations.

When you create your account, you will also have access to Console;
this will allow you to view all your transaction data interactively.
will allow you to view all your transaction data interactively,
with graphics and other resources in real time.

!!! info
    Sandbox environment is free, unlimited, and no deadline.
