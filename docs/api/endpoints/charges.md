---
title: Charges API (Legacy)
image: https://cc-og-image.vercel.app/Charges API.png?theme=dark&md=1&fontFamily=source-sans-pro&fontSize=125px&images=https://storage.googleapis.com/mesmerizing-matrix-1380/4geeks-docs-logo.svg
status: deprecated
---

!!! danger
    We don't recommend to use this endpoint to process cards. Use Payments API instead.

Charges API is a payment processing tool that allows businesses to accept and process online payments securely and efficiently. With this API, businesses can integrate payment processing functionalities into their websites, applications, and other software solutions.

It supports major payment methods, including credit cards, debit cards, and digital wallets. It also provides features such as real-time payment processing, automatic currency conversion, and fraud prevention.

## Create a charge

!!! info "Endpoint"
    `POST /v1/charges/simple/create/`

Charge a valid  endpoint crea un simple charge, en otras palabras, realiza un rebajo a la
tarjeta que venga en el POST, no está asignada a ningún `customer`.

=== "Shell"

``` shell
curl 'https://api.4geeks.io/v1/charges/simple/create/' \
    -X POST \
    -H 'authorization: bearer PdSKf04xi9LEcvcwIAdbWAXVg380Kz' \
    -F 'amount=800.55' \
    -F 'description=Desc for Simple' \
    -F 'entity_description=Desc for Simple' \
    -F 'currency=usd' \
    -F 'credit_card_number=4242424242424242' \
    -F 'credit_card_security_code_number=123' \
    -F 'exp_month=11' \
    -F 'exp_year=2020'
```


=== "Python"

``` python
gpayments.SimpleCharge.create(
    amount=10.99,
    description='Description',
    entity_description='Entity Description',
    currency='usd',
    credit_card_number='4242424242424242',
    credit_card_security_code_number=123,
    exp_month=12,
    exp_year=2020
)
```


Response:

```
HTTP 201 Created
```
