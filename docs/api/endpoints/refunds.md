---
title: Refunds API
image: https://cc-og-image.vercel.app/Refunds API.png?theme=dark&md=1&fontFamily=source-sans-pro&fontSize=125px&images=https://storage.googleapis.com/mesmerizing-matrix-1380/4geeks-docs-logo.svg
slug: /refunds
---

Refunds API is a tool that allows businesses to initiate and manage refunds for transactions made through their payment processing system. When a customer requests a refund, the Refunds API enables the business to process the refund quickly and easily, without having to manually handle the refund process.

This API handles the communication with the payment processor to ensure that the refund is processed correctly and in compliance with any relevant regulations.

## Create a refund

!!! info "Endpoint"
    `POST /v1/refunds/`

Sólo es posible reembolsar el 100% del monto de una transacción satisfactoria,
siempre y cuando ésta transacción no haya sido depositada a tu cuenta de banco aún. Una vez iniciado, un reembolso no se puede cancelar.

También puedes reembolsar una transacción directamente desde la [Consola](https://console.4geeks.io).


**Fields:**

| Parameter    |  Description                                        | Type   | Required     
| ------------ | :-------------------------------------------------: |:----:  | :-------:
| amount       |   Monto del reembolso                               | string | true
| charge_id    |   Identificador del Charge en cuestión              | string | true
| reason       |  `duplicate`, `fraudulent` o `requested_by_customer`| string | true


import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

<Tabs
  defaultValue="shell"
  values={[
    { label: 'Shell', value: 'shell', },
    { label: 'Python', value: 'py', },
    { label: 'Javascript', value: 'js', },
  ]
}>
<TabItem value="shell">

```shell
curl -X POST \
  https://api.4geeks.io/v1/refunds/ \
  -H 'Authorization: bearer GZe5XDYlwPEk4OW1o5XCjqIOFnVABk' \
  -F 'amount=10' \
  -F 'charge_id=1BSt6hCqnAMAMqhvMGiBxOWe' \
  -F 'reason=requested_by_customer'
```

</TabItem>
<TabItem value="py">

```py
gpayments.Refund.create(
    charge_id='1BSt6hCqnAMAMqhvMGiBxOWe',
    amount=10.99,
    reason='requested_by_customer'
)
```

</TabItem>
<TabItem value="js">

```js
await gpApi.charges.create({
  amount: 90.32,
  description: 'Plan 1 service charge',
  entity_description: 'Plan 1',
  currency: 'usd',
  credit_card_number: 4242424242424242,
  credit_card_security_code_number: 123,
  exp_month: 11,
  exp_year: 2020
})
```

</TabItem>
</Tabs>

Retorna:

```json
{
    "refund_id": "1BiPhgCqnAMsdzqhvCTntF7aD",
    "charge_id": "1BSt6hCqnAMAasd3vMGiBxOWe",
    "amount": "10.99",
    "currency": "usd",
    "reason": "requested_by_customer",
    "status": "succeeded",
    "test": true
}
```

---

## Get a refund

!!! info "Endpoint"
    `GET /v1/refunds/<refund_id>/`

This endpoint return a refund object.

Request:

```shell
curl https://api.4geeks.io/v1/refunds/1BiPhgCqnAMsdzqhvCTntF7aD \
     -X GET \
     -H 'authorization: bearer PdSKf04xi9LEcvcwIAdbWAXVg380Kz' \
```


Response:

```json
{
    "refund_id": "1BiPhgCqnAMsdzqhvCTntF7aD",
    "charge_id": "1BSt6hCqnAMAasd3vMGiBxOWe",
    "amount": "10.00",
    "currency": "usd",
    "reason": "duplicate",
    "status": "succeeded",
    "test": true
}
```

---

## List refunds

!!! info "Endpoint"
    `GET /v1/refunds/`

Este endpoint devuelve todos los reembolsos generados.

Request:

```shell
curl https://api.4geeks.io/v1/refunds/ \
  -X GET \
  -H 'authorization: bearer PdSKf04xi9LEcvcwIAdbWAXVg380Kz' \
```


Response:

```json
[
    {
        "refund_id": "1BiOrQCqNertMqhvvUMhJajE",
        "charge_id": "1BSt5sCqnAMAMqhvd871C1Vl",
        "amount": "10.00",
        "currency": "usd",
        "reason": "duplicate",
        "status": "succeeded",
        "test": true
    },
    {
        "refund_id": "1BiPDoCqnNerAMqhvSzxyFXl2",
        "charge_id": "1BSt5sCqnAMAMqhvd871C1Vl",
        "amount": "40.00",
        "currency": "usd",
        "reason": "fraudulent",
        "status": "succeeded",
        "test": true
    },
    {
        "refund_id": "1BiPeNCqnANerMqhvc4QUDeeK",
        "charge_id": "1BSt6hCqnAMAMqhvMNerxOWe",
        "amount": "10.00",
        "currency": "usd",
        "reason": "requested_by_customer",
        "status": "succeeded",
        "test": true
    },
    {
        "refund_id": "1BiPhgNernAMAMqhvCTntF7aD",
        "charge_id": "1BSt6hCqnAMAMqhvMGiBNerWe",
        "amount": "10.00",
        "currency": "usd",
        "reason": "null",
        "status": "succeeded",
        "test": true
    }
]
```
