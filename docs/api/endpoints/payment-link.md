---
title: Payment Links API
image: https://cc-og-image.vercel.app/Payment links API.png?theme=dark&md=1&fontFamily=source-sans-pro&fontSize=125px&images=https://storage.googleapis.com/mesmerizing-matrix-1380/4geeks-docs-logo.svg
slug: /payment-links
keywords:
  - payment links
---
Soon ...

<!--
Basically the Payment Links API help businesses to generate and customize the payment link with specific details such as the payment amount, currency, description and customer.

Once the payment link is generated, businesses can share it with their customers via email, social media, or other communication channels. When the customer clicks on the link, they are taken to a payment processing page where they can enter their payment details and complete the transaction.

All generated payment links implement 3DS by default.

## Create a payment link

:::info Endpoint
`POST /v1/links/`
:::

This endpoint generate a new payment link and build a secure checkout page, starting only from a [product](/api/products). Optionally, you can pass a customer object, in order to ask 4Geeks to send the generated payment link a customer's email.

**Fields:**

| Parameter    |  Description                                        | Type   | Required     
| ------------ | :-------------------------------------------------: |:----:  | :-------:
| product      |   A product to sell                                 | string | true
| customers    |   A customer id (if exists) or a list of customers including `first_name`, `last_name`, `email`. 4Geeks will merge this with a existing customer.                            | list or Object | false

**Request:**

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

<Tabs
defaultValue="shell"
values={[
{ label: 'Curl', value: 'shell', },
{ label: 'Python', value: 'py', },
{ label: 'Angular', value: 'ts', },
{ label: 'Javascript', value: 'js', },
]
}>
<TabItem value="shell">

```shell
curl --location 'https://api.4geeks.io/v1/links/' \
--data-raw '{
    "customers": [
        {
            "first_name": "Marco",
            "last_name":"Cruz",
            "email" : "test@test.com"
        }
    ],
    "product": "64de4526-1243-4125-b9ee-06683a145ce3"
}'
```

</TabItem>
<TabItem value="py">

```py
import requests

url = "https://api.4geeks.io/v1/links/"

payload = {
    "customers":{
                  "first_name": "Marco",
                  "last_name":"Cruz",
                  "email" : "marco.cruz+1@4geeks.io"
                },
    "product": "64de4526-1243-4125-b9ee-06683a145ce3"
}
headers = {}

response = requests.request("POST", url, headers=headers, data=payload)
```

</TabItem>
<TabItem value="ts">

```ts
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class PaymentLinkService {
  url = "https://api.4geeks.io/v1/links/";
  constructor(private http: HttpClient) {}

  createPaymentLink(): Observable<any> {
    var formdata = new FormData();
    formdata.append(
      "customers",
      '{"first_name": "Marco","last_name": "Cruz","email": "test@test.com"}'
    );
    formdata.append("product", "64de4526-1243-4125-b9ee-06683a145ce3");
    return this.http.post<any>(url, formdata);
  }
}
```

</TabItem>
<TabItem value="js">

```js
var raw = {
  customers: {
    first_name: "Marco",
    last_name: "Cruz",
    email: "test@test.com",
  },
  product: "64de4526-1243-4125-b9ee-06683a145ce3",
};

var requestOptions = {
  method: "POST",
  body: raw,
  redirect: "follow",
};

fetch("https://api.4geeks.io/V1/links/", requestOptions)
  .then((response) => response.text())
  .then((result) => console.log(result))
  .catch((error) => console.log("error", error));
```

</TabItem>
</Tabs>

**Response:**

In case you have created a payment link for multiple customers, it will return a
list of unique payment links for each customer.


```json
{
    "code": 201,
    "title": "Payment link created",
    "content": "The payment link was created correctly.",
    "type": "success",
    "data": [
      {
        "id": "50a5c880-f61b-4a9b-866a-471dbf0f4ec3",
        "customer": "33acc749-d017-4a6e-a379-5ba167472e21",
        "customer_name": "Marco Cruz",
        "link": "https://console.4geeks.io/pl/50a5c880-f61b-4a9b-866a-471dbf0f4ec3"
    },
    {
        "id": "50a5c880-f61b-4a9b-866a-471dbf0f4ec5",
        "customer": "33acc749-d017-4a6e-a379-5ba167472e22",
        "customer_name": "Juan Perez",
        "link": "https://console.4geeks.io/pl/50a5c880-f61b-4a9b-866a-471dbf0f4ec5"
    },
    {
        "id": "50a5c880-f61b-4a9b-866a-471dbf0f4ec4",
        "customer": "33acc749-d017-4a6e-a379-5ba167472e23",
        "customer_name": "Maria Aguilar",
        "link": "https://console.4geeks.io/pl/50a5c880-f61b-4a9b-866a-471dbf0f4ec4"
    }
    ]
}
```


## Get a payment link

:::info Endpoint
`GET /v1/links/<id>/`
:::

Los detalles de un Payment Link se pueden consultar de la siguiente manera:

<Tabs
defaultValue="shell"
values={[
{ label: 'Curl', value: 'shell', },
{ label: 'Python', value: 'py', },
{ label: 'Angular', value: 'ts', },
{ label: 'Javascript', value: 'js', },
]
}>
<TabItem value="shell">

```shell
curl --location 'https://api.4geeks.io/v1/links/50a5c880-f61b-4a9b-866a-471dbf0f4ec3'
```

</TabItem>
<TabItem value="py">

```py
import requests

url = "https://api.4geeks.io/v1/links/50a5c880-f61b-4a9b-866a-471dbf0f4ec3"

payload={}
headers = {}

response = requests.request("GET", url, headers=headers, data=payload)
```

</TabItem>
<TabItem value="ts">

```ts
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class PaymentLinkService {
  url = "https://api.4geeks.io/v1/links/";
  constructor(private http: HttpClient) {}

  detailsPL(id: String): Observable<any> {
    return this.http.get<any>(`${url}${id}/`);
  }
}
```

</TabItem>
<TabItem value="js">

```js
var requestOptions = {
  method: "GET",
  redirect: "follow",
};

fetch(
  "https://api.4geeks.io/v1/links/50a5c880-f61b-4a9b-866a-471dbf0f4ec3",
  requestOptions
)
  .then((response) => response.text())
  .then((result) => console.log(result))
  .catch((error) => console.log("error", error));
```

</TabItem>
</Tabs>


Response:

```json
{
    "id": "50a5c880-f61b-4a9b-866a-471dbf0f4ec3",
    "product": {
        "id": "64de4526-1243-4125-b9ee-06683a145ce3",
        "name": "Membresia Gym Musculitos",
        "short_description": null,
        "description": null,
        "stock": 0,
        "price": 8500.0,
        "currency": "CRC",
        "is_physical": false,
        "bar_code": null,
        "sku": null,
        "recurrence": {
            "recurrence": "period",
            "duration_period": 1
        },
        "images": {
            "images": [],
            "default": ""
        },
        "payment_link": false,
        "created_on": "2023-02-27T19:39:49.382019Z",
        "test": true,
        "total_price": 8500.0,
        "taxes": []
    },
    "customer": {
        "id": "33acc749-d017-4a6e-a379-5ba167472e21",
        "first_name": "Marco",
        "last_name": "Cruz",
        "email": "test@test.io",
        "phone": null,
        "photo": null,
        "country": null,
        "metadata": null,
        "created_on": "2023-02-27T19:46:10.038710Z",
        "test": true
    },
    "active": true,
    "recurring": true,
    "next_date_of_payment": "2023-04-27T19:46:10.048006Z",
    "status": "paid_period",
    "test": true,
    "created_on": "2023-02-27T19:46:10.048343Z",
    "payment_logs": [
        {
            "id": "e41e7554-2df6-43da-b60c-27d5298061ff",
            "product": "64de4526-1243-4125-b9ee-06683a145ce3",
            "customer": "33acc749-d017-4a6e-a379-5ba167472e21",
            "amount": 8500.0,
            "fee": 712.2,
            "currency": "CRC",
            "description": null,
            "statement_description": "RAMS",
            "status": "succeeded",
            "capture": true,
            "created_on": "2023-02-27T19:57:09.128301Z",
            "refund": null,
            "captured": null,
            "test": true,
            "card_last4": "4242",
            "card_exp_month": "12",
            "card_exp_year": "2025",
            "card_country": "US",
            "card_brand": "visa",
            "net": 7787.8,
            "net_currency": "CRC"
        },
        {
            "id": "58d100dc-1a06-4cec-805c-c8fc5c63324d",
            "product": "64de4526-1243-4125-b9ee-06683a145ce3",
            "customer": "33acc749-d017-4a6e-a379-5ba167472e21",
            "amount": 8500.0,
            "fee": 712.2,
            "currency": "CRC",
            "description": null,
            "statement_description": "RAMS",
            "status": "succeeded",
            "capture": true,
            "created_on": "2023-02-27T19:46:26.878988Z",
            "refund": null,
            "captured": null,
            "test": true,
            "card_last4": "4242",
            "card_exp_month": "5",
            "card_exp_year": "2026",
            "card_country": "US",
            "card_brand": "visa",
            "net": 7787.8,
            "net_currency": "CRC"
        }
    ]
}
```


## Deactivate or activate a payment link

:::info Endpoint
Endpoint: `PUT /v1/links/<id>/`
:::

La activación o desactivación de un Payment Link se realiza dela siguiente forma

<Tabs
defaultValue="shell"
values={[
{ label: 'Curl', value: 'shell', },
{ label: 'Python', value: 'py', },
{ label: 'Angular', value: 'ts', },
{ label: 'Javascript', value: 'js', },
]
}>
<TabItem value="shell">

```shell
curl --location --request PUT 'https://api.4geeks.io/v1/links/0719cdc1-a72c-43ce-9262-38548faff428/' \
--form 'active="false"''
```

</TabItem>
<TabItem value="py">

```py
import requests

url = "https://api.4geeks.io/v1/links/0719cdc1-a72c-43ce-9262-38548faff428/"

payload={'active': 'false'}
files=[

]
headers = {}

response = requests.request("PUT", url, headers=headers, data=payload, files=files)

```

</TabItem>
<TabItem value="ts">

```ts
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class PaymentLinkService {
  url = "https://api.4geeks.io/v1/links/";
  constructor(private http: HttpClient) {}

  desactiveLink(id: String): Observable<any> {
    var formdata = new FormData();
    formdata.append("active", false);
    return this.http.put<any>(`${url}${id}/`, formdata);
  }

  activeLink(id: String): Observable<any> {
    var formdata = new FormData();
    formdata.append("active", true);
    return this.http.put<any>(`${url}${id}/`, formdata);
  }
}
```

</TabItem>
<TabItem value="js">

```js
var formdata = new FormData();
formdata.append("active", "false");

var requestOptions = {
  method: 'PUT',
  body: formdata,
  redirect: 'follow'
};

fetch(
  "https://api.4geeks.io/v1/links/0719cdc1-a72c-43ce-9262-38548faff428/",
  requestOptions
)
  .then((response) => response.text())
  .then((result) => console.log(result))
  .catch((error) => console.log("error", error));
```

</TabItem>
</Tabs>

El servidor te retornara la siguiente respuesta y en caso de necesitarlo en el campo `data` te devuelve los valores actualizados del Payment Link

```json
{
    "code": 200,
    "title": "Payment Link Actualizado",
    "content": "El Payment Link fue actualizado correctamente.",
    "type": "success",
    "data": {
        "id": "0719cdc1-a72c-43ce-9262-38548faff428",
        "product": {
            "id": "a1327713-f7cb-4318-831f-9c3da074e603",
            "name": "testeoo",
            "short_description": null,
            "description": null,
            "stock": 0,
            "price": 10000,
            "currency": "CRC",
            "is_physical": false,
            "bar_code": null,
            "sku": null,
            "recurrence": {
                "payday": "15",
                "recurrence": "specific_billing_day",
                "date_finish": ""
            },
            "images": {
                "images": [],
                "default": ""
            },
            "payment_link": true,
            "created_on": "2023-02-26T06:57:08.256036Z",
            "test": true,
            "total_price": 10000,
            "taxes": []
        },
        "customer": {
            "id": "80f0250f-dfd1-44e3-af4a-824f81498fa9",
            "first_name": "Marco",
            "last_name": "Cruz",
            "email": "marco.cruz@4geeks.io",
            "phone": "123456789",
            "photo": "",
            "country": "Costa Rica",
            "metadata": null,
            "created_on": "2023-02-26T06:56:07.152267Z",
            "test": true
        },
        "active": false,
        "recurring": true,
        "next_date_of_payment": "2023-03-15T00:00:00Z",
        "status": "cancelled",
        "test": true,
        "created_on": "2023-02-27T18:34:49.328392Z",
        "payment_logs": [
            {
                "id": "43321223-df14-49bd-95c8-6ccc6f2b9658",
                "product": "a1327713-f7cb-4318-831f-9c3da074e603",
                "customer": "80f0250f-dfd1-44e3-af4a-824f81498fa9",
                "amount": 10000,
                "fee": 819.58,
                "currency": "CRC",
                "description": null,
                "statement_description": "CONDORITOSA",
                "status": "succeeded",
                "capture": true,
                "created_on": "2023-02-27T18:35:40.905923Z",
                "refund": null,
                "captured": null,
                "test": true,
                "card_last4": "4242",
                "card_exp_month": "12",
                "card_exp_year": "2025",
                "card_country": "US",
                "card_brand": "visa",
                "net": 9180.42,
                "net_currency": "CRC"
            }
        ]
    }
}
```

Como notaras el campo `active` se actualizo pasando a un estado de inactivo el Payment Link, no se notificara en las fechas de cobro, ni entrara en estado de moroso, asi mismo no se permitirá pagarlo.

## List payment links

:::info Endpoint
`GET /v1/links/`
:::

Para consultar la lista de Payment Link se realiza la siguiente consulta:

<Tabs
defaultValue="shell"
values={[
{ label: 'Curl', value: 'shell', },
{ label: 'Python', value: 'py', },
{ label: 'Angular', value: 'ts', },
{ label: 'Javascript', value: 'js', },
]
}>
<TabItem value="shell">

```shell
curl --location 'https://api.4geeks.io/v1/links/'
```

</TabItem>
<TabItem value="py">

```py
import requests

url = "https://api.4geeks.io/v1/links/"

payload={}
headers = {}

response = requests.request("GET", url, headers=headers, data=payload)
```

</TabItem>
<TabItem value="ts">

```ts
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class PaymentLinkService {
  url = "https://api.4geeks.io/v1/links/";
  constructor(private http: HttpClient) {}

  listPaymentLinks(): Observable<any> {
    return this.http.get<any>(`${url}`);
  }
}
```

</TabItem>
<TabItem value="js">

```js
var requestOptions = {
  method: "GET",
  redirect: "follow",
};

fetch("https://api.4geeks.io/v1/", requestOptions)
  .then((response) => response.text())
  .then((result) => console.log(result))
  .catch((error) => console.log("error", error));
```

</TabItem>
</Tabs>

Respuesta del servidor

```json
[
    {
        "id": "0719cdc1-a72c-43ce-9262-38548faff428",
        "amount": 10000,
        "total_amount": 10000,
        "currency": "CRC",
        "product": "a1327713-f7cb-4318-831f-9c3da074e603",
        "client_name": "Marco Cruz",
        "email": "marco.cruz@4geeks.io",
        "customer": "80f0250f-dfd1-44e3-af4a-824f81498fa9",
        "active": false,
        "recurring": true,
        "next_date_of_payment": "2023-03-15T00:00:00Z",
        "status": "cancelled",
        "test": true,
        "created_on": "2023-02-27T18:34:49.328392Z"
    },
    {
        "id": "439b6432-d988-42c4-b6c5-b3e930da10df",
        "amount": 10000,
        "total_amount": 10000,
        "currency": "CRC",
        "product": "a1327713-f7cb-4318-831f-9c3da074e603",
        "client_name": "Marco Cruz",
        "email": "marco.cruz@4geeks.io",
        "customer": "80f0250f-dfd1-44e3-af4a-824f81498fa9",
        "active": true,
        "recurring": true,
        "next_date_of_payment": "2023-09-15T00:00:00Z",
        "status": "paid_period",
        "test": true,
        "created_on": "2023-02-26T06:57:38.824557Z"
    },
    {
        "id": "04c05460-3537-4089-9c3d-732341497d06",
        "amount": 55,
        "total_amount": 73.15,
        "currency": "USD",
        "product": "5c46d97b-22d5-4917-8833-3a61385d7c27",
        "client_name": "Marco Cruz",
        "email": "marco.cruz@4geeks.io",
        "customer": "80f0250f-dfd1-44e3-af4a-824f81498fa9",
        "active": true,
        "recurring": false,
        "next_date_of_payment": "2023-02-25T23:47:51.999177Z",
        "status": "pending",
        "test": true,
        "created_on": "2023-02-25T23:47:55.007576Z"
    }
]
```
-->
