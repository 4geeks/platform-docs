---
title: Payments API
status: new
---

Payments API is a payment processing tool that allows businesses to accept and process online payments securely and efficiently. With this API, businesses can integrate payment processing functionalities into their websites, applications, and other software solutions.

It supports major payment methods, including credit cards, debit cards, and digital wallets. It also provides features such as real-time payment processing, automatic currency conversion, and fraud prevention.

All charges request [3DS](../../billing/features/charges.md/#charges-with-3d-secure) to cardholder's bank by default.

## Create a payment request

!!! info "Endpoint"
    `POST /v1/payments/`

This endpoint builds a single secure billing page, based on the following parameters.
Please note that you must direct the cardholder to a generated page, where they will
enter payment method data. We will send the transaction response to `return_url`.

Fields:

| Parameter    |  Description                                        | Type   | Required     
| :------------: | ------------------------------------------------- |:----:  | :-------:
| amount       |   Total amount to charge                            | string | true
| description  |   A description                                     | string | true
| currency     |  A valid currency code ISO-2, for example `USD`, `CAD`, `CRC`.| string | true
| items        |  Name of product or service. It's an informational label only. | array | true
| return_url   |  We'll redirect to this url after payment, and `id_payment` is concatenated to the URL, so you can consult the transaction status. | string | true


Request:

```shell
curl 'https://api.4geeks.io/v1/payments/' \
    -X POST \
    -H 'authorization: bearer PdSKf04xi9LEcvcwIAdbWAXVg380Kz' \
    -F 'amount=500' \
    -F 'description=Some description here' \
    -F 'currency=usd' \
    -F 'items: ["Product name 1","Product name 2","Product name N"]'
    -F 'return_url: https://example.com/result'
```


Response:

```json
{
    "code": 202,
    "title": "Action required",
    "content": "Show the checkout page to complete payment.",
    "data": {
        "redirect": "https://console.4geeks.io/checkout/?data=eyJyZXR1cm5fdXJsIjogImh0dHA6Ly9sb2NhbGhvc3Q6NDIwMC9wYXktY29tcGxldGUiLCAidGVzdCI6IHRydWUsICJoYXNfY3VzdG9tZXIiOiBmYWxzZSwgImNvbXBhbnlfZW1haWwiOiAicmFuZHkubWFydGluZXpANGdlZWtzLmlvIiwgInByb2R1Y3QiOiB7InRvdGFsX3ByaWNlIjogMSwgInByaWNlIjogMSwgImN1cnJlbmN5IjogIlVTRCIsICJuYW1lIjogIlBhZ28gZGUgYXJ0aWN1bG9zIHZhcmlvcyIsICJkZXNjcmlwdGlvbiI6IFsiQ2FtaXNhIiwgIkdvcnJhIiwgIlNhbmRhbGlhcyJdfSwgInN0YXRlbWVudF9kZXNjcmlwdG9yIjogWyJDT05ET1JJVE9TQSJdLCAiY29tcGFueV9uYW1lIjogIkNvbmRvcml0byBTLkEifQ=="
    }
}
```



## Get a payment

!!! info "Endpoint"
    `GET /v1/payments/<id>`

Consult this endpoint to get a transaction detail.

## Get all payments

!!! info "Endpoint"
    `GET /v1/payments`
