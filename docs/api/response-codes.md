---
title: Response codes
image: https://cc-og-image.vercel.app/Códigos de respuesta.png?theme=dark&md=1&fontFamily=source-sans-pro&fontSize=125px&images=https://storage.googleapis.com/mesmerizing-matrix-1380/4geeks-docs-logo.svg
slug: /response-codes
---

The API returns response messages accompanied by HTTP codes. Here we describe the codes and a short description.


| Response code|  Description             |
| ------------ | :----------------------: |
| 201          |   Created                |
| 204          |   No Content             |
| 400          |   Bad Request            |
| 401          |   Unauthorized           |
| 403          |   Forbidden              |
| 404          |   Not Found              |
| 405          |   Method Not Allowed     |
| 429          |   Too Many Requests      |
| 500          |   Internal Server Error  |
| 503          |   Service Unavailable    |
