---
title: Testing cards
image: https://cc-og-image.vercel.app/Testing cards.png?theme=dark&md=1&fontFamily=source-sans-pro&fontSize=125px&images=https://storage.googleapis.com/mesmerizing-matrix-1380/4geeks-docs-logo.svg
slug: /testing-cards
---


Listed below are some numbers and brand of cards for testing purposes, so that you can simulate a real payment flow. Keep in mind that `exp_year` and `exp_month` must point to a future date, and `credit_card_security_code_number` can be any positive numeric value.


| Número de tarjeta        |      Marca
| ------------------------ | :-----------: |
| 4242424242424242         |       VISA    |
| 5555555555554444         |   MasterCard  |
| 378282246310005          |AmericanExpress|


If you want to **force errors** during the payment process, please use the following cards. You can also try invalid numbers in `exp_year`, `exp_month` and `credit_card_security_code_number`, either by putting past dates or invalid months.


| Número de tarjeta        |      Error
| ------------------------ | :----------------: |
| 4000000000000127         | CVC incorrecto     |
| 4000000000000002         | Tarjeta denegada   |
| 4000000000000069         | Tarjeta expirada   |
