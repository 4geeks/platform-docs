# Platform Docs


### Overview
This project houses the documentation for the entire 4Geeks platform, built using MkDocs.

### Prerequisites
* **Python 3**: Ensure you have Python 3 installed on your system.
* **Virtual Environment**: Recommended to isolate project dependencies.

### Setup
1. **Clone the repository**:
   ```bash
   git clone https://github.com/your-username/platform-docs.git
   cd platform-docs
   ```
2. **Create a virtual environment**:
   ```bash
   python -m venv venv
   ```
3. **Activate the virtual environment**:
   * **Windows**: `venv\Scripts\activate`
   * **macOS/Linux**: `source venv/bin/activate`
4. **Install dependencies**:
   ```bash
   pip install -r requirements.txt
   ```

### Running the Documentation
To serve the documentation locally, use the following command:
```bash
mkdocs serve
```
This will start a development server at `http://127.0.0.1:8000`.

### Building the Documentation
To build the documentation for production, use the following command:
```bash
mkdocs build
```
This will create a `site` directory containing the built HTML files.

### Additional Notes
* **MkDocs Configuration**: The project configuration is defined in `mkdocs.yml`.
* **Content**: Documentation content is located in the `docs` directory.
* **Themes**: You can customize the appearance of your documentation using MkDocs themes.

**For more information on MkDocs, please refer to the official documentation:** [https://www.mkdocs.org/](https://www.mkdocs.org/)
